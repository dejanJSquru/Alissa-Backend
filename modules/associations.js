const db = ModuleRegistry.import('db').Registry;

db.Role.belongsToMany(db.User, {through: 'UserInRole'});
db.User.belongsToMany(db.Role, {through: 'UserInRole'});