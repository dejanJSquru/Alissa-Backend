'use strict';

let error = (message, status, config) => {
    let error = new Error(message);
    error.status = status;
    error.message = message;
    Object.assign(error, config || {});
    return error;
};

module.exports.error = (message, config) => {
    return error(message, status, config);
};

module.exports.notAuthorizedError = (message, config) => {
    return error(message, 401, config);
};

module.exports.internalServerError = (message, config) => {
    return error(message, 500, config);
};

module.exports.notImplemented = (message, config) => {
    return error(message, 501, config);
};

module.exports.notFound = (message, config) => {
    return error(message, 404, config);
};

module.exports.badRequest = (message, config) => {
    return error(message, 400, config);
};

module.exports.validationFailed = (message, config) => {
    config = config || {};
    config.validation = message;
    config.message = "Validation failed";
    return error(message, 400, config);
};

module.exports.unauthorized = (message, config) => {
    return error(message, 401, config);
};

module.exports.forbidden = (message, config) => {
    return error(message, 403, config);
};