const ErrorService = require('./services/error.service');

module.exports = () => {
    return {
        ErrorService
    };
};