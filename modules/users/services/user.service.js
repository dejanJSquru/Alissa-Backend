const ModuleRegistry = global.ModuleRegistry;
const db = ModuleRegistry.import('db').Registry;

const findById = (id) => {
    return db.User.findById(id, {
        attributes: {
            exclude: ['password']
        }
    });
};

const findAll = (query) => {
    return db.User.findAll({
        attributes: {
            exclude: ['password']
        }
    });
};

const create = (body) => {
    return db.User.create({
        email: body.email,
        password: body.password
    });
};

const update = (id, body) => {
    return db.User.update({
        email: body.email,
        password: body.password
    }, {
        where: {
            id: id
        }
    });
};

const destroy = (id) => {
    return db.User.destroy({
        where: {
            id: id
        }
    });
};

module.exports = {
    findAll,
    findById,
    create,
    update,
    destroy
}