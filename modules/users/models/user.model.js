'use strict';
const schema = require('./user.schema');

module.exports = (sequelize, DataTypes) => {
    const User = sequelize.define('User', schema(DataTypes), {
        timestamps: false,
        paranoid: false,
        freezeTableName: true
    });
    return User;
};
