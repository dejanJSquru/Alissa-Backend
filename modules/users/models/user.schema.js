module.exports = (DataTypes) => {
    return {
        email: {
            type: DataTypes.STRING,
            unique: true,
            notNull: true
        },
        password: DataTypes.STRING,
        settings: DataTypes.JSON
    };
};
