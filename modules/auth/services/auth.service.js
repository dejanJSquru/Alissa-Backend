const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const db = ModuleRegistry.import('db').Registry;
const ErrorService = ModuleRegistry.import('error').ErrorService;

const register = (body) => {

    return db.User.findOne({
            where: {
                email: body.email
            }
        })
        .then((response) => {
            if (response) {
                throw ErrorService.badRequest('Email already exists.');
            }
            const hashedPassword = bcrypt.hashSync(body.password, 8);

            return db.User.create({
                    email: body.email,
                    password: hashedPassword
                })
                .then((response) => {
                    const token = jwt.sign({
                        id: response.id
                    }, process.env.ALISSA_SECRET || 'mysecret', {
                        expiresIn: 86400 // expires in 24 hours
                    });
                    return token;
                })
        })
};

const getMe = (id) => {

    return db.User.findById(id, {
        attributes: {
            exclude: ['password']
        }
    })
};

const login = (body) => {

    return db.User.findOne({
            where: {
                email: body.email
            }
        })
        .then((response) => {
            if (!response) {
                throw ErrorService.badRequest('Invalid entry');
            }

            const passwordIsValid = bcrypt.compareSync(body.password, response.password);
            if (!passwordIsValid) {
                throw ErrorService.badRequest('Invalid entry');
            };

            const token = jwt.sign({
                id: response.id
            }, process.env.ALISSA_SECRET || 'mysecret', {
                expiresIn: 86400 // expires in 24 hours
            });
            return token;
        })
};

module.exports = {
    register,
    getMe,
    login
};