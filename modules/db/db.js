'use strict';
const Sequelize = require('sequelize');
const fs = require('fs');
const path = require('path');
const recursive = require('recursive-readdir');

let db = {}, sequelize;

exports.init = function (config) {

    let models = [];

    sequelize = new Sequelize(
        config.database,
        config.username,
        config.password, {
            host: config.host,
            port: config.port,
            dialect: config.dialect,
            define: {
                timestamps: false
            },
            logging: config.logDB ? console.log : false
        });
    
        let modelsPath = path.resolve(__dirname, '../');

    recursive(modelsPath, function (err, files) {

        if (err) {
            return;
        }

        let filteredFiles = files.filter(function (file) {
            return (file.indexOf(".model.") > 0);
        });

        filteredFiles.forEach(function (file) {
            let model = sequelize["import"](file);
            db[model.name] = model;
            models.push(model);
        });

        db.sequelize = sequelize;
        db.Sequelize = Sequelize;

        Object.keys(db).forEach(function (modelName) {
            if (db[modelName].hasOwnProperty('associate')) {
                db[modelName].associate(db);
            }
        });

    });

    //Executing check database connection on db init (prevents bad configs)
    return sequelize.query('SELECT 1 from user')
        .then(() => {
            return db;
        });
};

exports.Registry = db;