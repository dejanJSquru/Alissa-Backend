'use strict';
const schema = require('./role.schema');

module.exports = (sequelize, DataTypes) => {
    const Role = sequelize.define('Role', schema(DataTypes), {
        timestamps: false,
        paranoid: false,
        freezeTableName: true
    });
    return Role;
};