module.exports = (DataTypes) => {
    return {
        name: DataTypes.STRING,
        allowNull: false,
    };
};