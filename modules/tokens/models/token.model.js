'use strict';
const schema = require('./token.schema');

module.exports = (sequelize, DataTypes) => {
    const UserToken = sequelize.define('UserToken', schema(DataTypes), {
        timestamps: false,
        paranoid: false,
        freezeTableName: true
    });
    return UserToken;
};