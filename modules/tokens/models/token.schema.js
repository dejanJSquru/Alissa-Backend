module.exports = (DataTypes) => {
    return {
        token: DataTypes.JSON,
        expires: DataTypes.DATE
    };
};