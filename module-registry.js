'use strict';

const fs = require('fs'),
    path = require('path'),
    Module = require('./Module');

let registry = {};

/**
 * Helpers
 */

function getDirectories(src) {
    return fs.readdirSync(src).filter(function (entry) {
        return fs.statSync(path.resolve(src, entry)).isDirectory();
    })
        .map((entry) => {
            return path.resolve(src, entry);
        });
}

/**
 * Scans given root and registers modules.
 * @param root Root directory to scan.
 */
exports.scan = function (root) {

    root = root || "./modules";

    let directories = getDirectories(path.resolve(__dirname, root));

    directories.forEach((directory) => {
        let config;
        try {
            config = require(`${directory}/module.json`);
            config.path = directory;
            this.register(config);
        }
        catch (e) {
            console.log(`INFO: Cannot find module.json file for ${directory}  so ignoring`);
        }
    });

};

/**
 * Registers module using given configuration.
 * @param config Module json configuration.
 */
exports.register = function (config) {

    if (registry[config.name]) {
        throw new Error(`Module with name ${config.name} is already registered`);
    }

    let module = new Module(config.name, config);
    registry[module.name] = module;
};

/**
 * Retrieves module with specific name.
 * @param name
 * @returns {*}
 */
exports.module = function (name) {
    return registry[name];
};

/**
 * Imports module with specific name.
 * @param name
 * @returns {*}
 */
exports.import = function (name) {
    let module = this.module(name);
    if (!module) {
        throw new Error(`Cannot find module with name ${name}`);
    }

    let main = require(`${module.path}/${module.entry}`);
    return main(registry);
};

exports.modules = function () {
    return registry;
};

exports.resources = () => {
    let resources = [];
    Object.keys(registry).forEach(function (key) {
        const item = registry[key];
        if (item.resources) {
            resources = resources.concat(item.resources);
        }
    });
    return resources;
};