const express = require('express');
const controller = require('./user.controller');
const router = express.Router();

const { check, validationResult } = require('express-validator/check');
const { matchedData, sanitize } = require('express-validator/filter');

router.get('/',controller.findAll);
router.get('/:id',controller.findById);
router.post('/',[check('email').isEmail().withMessage('Invalid email')],controller.create);
router.put('/:id/',[check('email').isEmail().withMessage('Invalid email')],controller.update);
router.delete('/:id',controller.destroy);

module.exports = router;
