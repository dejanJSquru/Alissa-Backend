
const ModuleRegistry = global.ModuleRegistry;
const UserService = ModuleRegistry.import('user').UserService;

const { check, validationResult } = require('express-validator/check');
const { matchedData, sanitize } = require('express-validator/filter');

const findAll = (req, res, next) => {
    return UserService.findAll(req.query)
        .then((response) => {
            res.json(response);
        }).catch(next);
};

const findById = (req, res, next) => {
    return UserService.findById(req.params.id)
        .then((response) => {
            res.json(response);
        }).catch(next);
};

const create = (req, res, next) => {

    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.mapped() });
    }

    let body=req.body;
    if(!body){
        body=req.query;
    }
    return UserService.create(body)
        .then((response) => {
            res.json(response);
        }).catch(next);
};

const update = (req, res, next) => {

    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.mapped() });
    }
    
    let body=req.body;
    if(!body){
        body=req.query;
    }
    return UserService.update(req.params.id, body)
        .then((response) => {
            res.json(response);
        }).catch(next);
};

const destroy = (req, res, next) => {
    return UserService.destroy(req.params.id)
        .then((response) => {
            res.json(response);
        }).catch(next);
};

module.exports = {
    findAll,
    findById,
    create,
    update,
    destroy
}