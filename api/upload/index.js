const express = require('express');
const router = express.Router();
const multer = require('multer');
const bodyParser = require('body-parser');


const Storage = multer.diskStorage({
    destination: function(req, file, callback) {
        callback(null, "./api/upload/images");
    },
    filename: function(req, file, callback) {
        callback(null, Date.now() + "_" + file.originalname);
    }
});

const upload = multer({
    storage: Storage
}).array("imgUploader", 3); //Field name and max count

router.get('/', function(req, res) {
    res.sendFile(__dirname + "/index.html");
});
router.post('/', function(req, res) {
    upload(req, res, function(err) {
        if (err) {
            return res.end("Something went wrong!");
        }
        return res.end("File uploaded sucessfully!.");
    });
});

module.exports = router;