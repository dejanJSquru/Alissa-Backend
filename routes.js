const ModuleRegistry=global.ModuleRegistry;


module.exports= (app)=>{ 
    app.use('/api/user', require('./api/user'));
    app.use('/auth', require('./auth'));
    app.use('/upload', require('./api/upload'));
}