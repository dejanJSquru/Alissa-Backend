const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');

const controller = require('./auth.controller');
const VerifyToken = require('./verify.token');

const { check, validationResult } = require('express-validator/check');
const { matchedData, sanitize } = require('express-validator/filter');

router.post('/register', [check('email').isEmail().withMessage('Invalid email')], controller.register);
router.get('/me', VerifyToken, controller.getMe);
router.post('/login', controller.login);
router.post('/logout', controller.logout);

module.exports = router;