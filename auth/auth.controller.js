const ModuleRegistry = global.ModuleRegistry;
const AuthService = ModuleRegistry.import('auth').AuthService;

const {
    check,
    validationResult
} = require('express-validator/check');
const {
    matchedData,
    sanitize
} = require('express-validator/filter');

const register = (req, res, next) => {

    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({
            errors: errors.mapped()
        });
    }

    return AuthService.register(req.body)
        .then((response) => {
            res.json(response);
        })
        .catch(next);
};

const getMe = (req, res, next) => {

    return AuthService.getMe(req.userId)
        .then((response) => {
            if (!response) res.status(404).send("No user found.");

            res.status(200).send(response);
        })
        .catch(next);
};

const login = (req, res, next) => {
    
    return AuthService.login(req.body)
        .then((response) => {
            res.json(response);
        })
        .catch(next);
};

const logout = (req, res) => {
    res.status(200).send({
        auth: false,
        token: null
    });
};

module.exports = {
    register,
    getMe,
    login,
    logout
}