const express = require('express');
const ModuleRegistry = require('./module-registry');
const http = require('http');
const app = express();
const ErrorHandler = require('./error-handler');
const bodyParser = require('body-parser');


ModuleRegistry.scan();
global.ModuleRegistry = ModuleRegistry;
const config = require('./config/config.json')[process.env.NODE_ENV || 'development'];
const db = ModuleRegistry.import('db');
db.init(config);
app.server = http.createServer(app);

app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(bodyParser.json());
require('./routes')(app);

app.use((req, res) => {
    res.status(404).json({
        message: 'Route not found',
        status: 404,
        url: req.originalUrl
    })
});
app.use(ErrorHandler);

app.server.listen(4300, () => {
    console.log(`Started on port ${app.server.address().port}`);
});

module.exports = app;