'use strict';

class Module {
    constructor(name, config) {
        if (!name || typeof name !== 'string') {
            throw new Error('Module "name" property is required and has to be a string');
        }

        if (!config.entry) {
            throw new Error('Module "entry" property is required');
        }

        if (!config.path) {
            throw new Error('Module "path" property is required');
        }


        this.name = this.key = name;
        this.version = config.version || '0.0.0';
        this.entry = config.entry;
        this.group = config.group;
        this.path = config.path;
        this.resources = config.resources;
    }
}
module.exports = Module;