
function getStatusFromErrorType(err) {
    switch (err.name) {
        case 'SequelizeEmptyResultError':
            return {
                message: "Resource not found",
                code: 404
            };
        case 'SequelizeQueryError':
            return {
                message: "Bad request",
                code: 400
            };
        case 'SequelizeValidationError':
            return {
                message: "Bad request",
                code: 400
            };
        case 'SequelizeUniqueConstraintError':
            return {
                message: "Bad request",
                code: 400
            };
        case 'SequelizeForeignKeyConstraintError':
            return {
                message: "Bad request",
                code: 400
            };
        case 'SequelizeDatabaseError':
            return {
                message: "Bad request",
                code: 400
            };
        default:
            return {
                message: "Internal server error",
                code: 500
            }
    }
}

const env = process.env.NODE_ENV || 'development';


module.exports = (err, req, res, next) => {
    if (err && !err.status && typeof err === 'object') {
        let status = getStatusFromErrorType(err);
        err.status = status.code;
        err.message = status.message;
    } else if (typeof err === 'string') {
        err = {
            status: getStatusFromErrorType({
                name: err
            }),
            message: err
        };
    }

    res.status(err.status).json({
        message: err.message,
        status: err.status,
        validation: err.validation,
        url: req.originalUrl,
        stack: env !== 'production' ? err.stack : undefined
    });
};